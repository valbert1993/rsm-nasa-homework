# RSM NASA – Homework!

  <a href="https://expressjs.com"><img src="https://badgen.net/badge/express/4.16.1" alt="Express"></a>


## Install dependencies

    $ npm install
or

    $ yarn


## Running the project

    $ npm run start
or

    $ yarn start

## Servers

    - local server http://localhost:3000/


const express = require('express');
const axios = require('axios');
const env = require('../config/env.js');
const router = express.Router();

/* GET home page. */
router.get('/', async(req, res) => {
  const location = req.query.location;
  if (location){
    const coor = location.split(',')
    let imgs = [];
    let date = new Date();
    do {
      let response = await axios.get(
          `${env.NASA_URL}?lon=${coor[1]}&lat=${coor[0]}&date=${date.toISOString().split('T')[0]}&api_key=${env.NASA_KEY}&dim=0.5`
      ).catch((error) => {
        if (error.response.status !== 404){
          return res.render('index', {text_error: error.response.data.msg});
        }
      });
      if (response){
        imgs.push({
          date: date.toISOString().split('T')[0],
          url : response.data.url
        });
      }
      date.setMonth(date.getMonth() - 1);
      console.log(imgs.length);
    }while ((imgs.length < 9));
    return res.render('index', {
      images: imgs
    });
  }

  return res.render('index');
});

module.exports = router;

